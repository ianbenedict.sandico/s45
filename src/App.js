//destructured react, remove React from <React.Fragment>
import { Fragment, useState, useEffect } from 'react';
// import React from 'react';

import './App.css';
import AppNavbar from './components/AppNavbar';

//Pages
import Home from './pages/Home';
import Courses from './pages/Courses'
import Register from './pages/Register';
import Login from './pages/Login';
import Error from './pages/Error';
import Logout from './pages/Logout';
import AddCourse from './pages/AddCourse';

//routing components
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';

// import Banner from './components/Banner';
// import Highlights from './components/Highlights';

// import Counter from './components/Counter';

//Bootstrap
import { Container } from 'react-bootstrap';

//React Context
import { UserProvider } from './UserContext';

//Multiple elements below, need to wrap in a parent element add <React.Fragment>
// <AppNavbar />
// <Banner />

/*
Browser Router component will enable us to simulate page navigation by synchronizing the shown content and the shown URL in th web browser

Switch component then declares with Route we can go to

Route component will render components within the switch container based on the defined route

exact property disables the partial matching for a route and makes sure that it only returns the route if the path is an exact match to the current url

If exact and path is missing, the Route component will make it undefined route and will be loaded to a specified component
*/

function App() {

	//Add a state hook for user,
	//The getItem() method returns value of the specified Storage Object item

	const[user, setUser] = useState({ 
		accessToken: localStorage.getItem('accessToken'),
		email: localStorage.getItem('email'),
		isAdmin: localStorage.getItem('isAdmin') == 'true'
	})

	//Function for clearing localStorage on logout
	const unsetUser = () => {
		localStorage.clear()
	}

	//Used to check if the user information is properly stored upon login and the localStorage information is cleared upon Logout
	useEffect(()=>{
		// console.log(user);
		// console.log(localStorage)
	}, [user])

//Provider component that allows consuming components to subcribe to context changes
  return (

  	<UserProvider value={ {user, setUser, unsetUser} }>
	  	<Router>
		 <AppNavbar />
		 <Container>
		   <Switch>
		 	   <Route exact path="/" component={Home} />
			   <Route exact path="/courses" component={Courses} />
			   <Route exact path="/addCourse" component={AddCourse} />
			   <Route exact path="/register" component={Register} />
			   <Route exact path="/login" component={Login} />
			   <Route exact path="/logout" component={Logout} />
			   <Route path = "/" component = {Error} />
		   </Switch>
		  </Container>
		</Router>
	</UserProvider>
  );
}

export default App;

/*
NOTES:

With the React Fragment component, we can group multiple components and aovid adding extra code
<Fragment> is preferred over <></> (shortcut syntax) because it is not universal and can cause problems in some other editors

JSX Syntax
JSX, or Javascript XML is an extension to the syntax of JS. It allows us to write HTML like syntax within our react js projects and it includes JS features as well. 

/*
Install the Js(Babel) linting for code readability
1.Ctrl + Shift + P
2. In the input field type the word "install" and select the "package control: install package"
option to trigger an installation of an add on feature
3. Type "babel" in the input field to be installed
*/


/*
Hooks
1.useState- State changes of data, rapid changes
2.useEffect- Where we can add a "side effect to browser", and it is rended everytime
3.useContexts- used to unpack our react context. same with props, pass data, it is global state.

React Context is a global state to the app. It is a way to make a particular data available to all the components  no matter how they are nested. Context helps you to broadcast the data and changes happening to that data to all components. 
*/


/*
Error
400+ Client	React
500+ Server	Express

*/