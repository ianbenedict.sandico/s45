import { useState, useEffect, useContext } from 'react'
import { Button, Container, Row, Col, Form } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2'

export default function AddCourse(){
	const { user } = useContext(UserContext)
	const [name, setName] = useState('')
	const [description,	setDescription] = useState('')
	const [price, setPrice] = useState(0)

	function createCourse(e){
		e.preventDefault() 
		fetch('http://localhost:4000/courses/', {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${user.accessToken}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
				})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				
				Swal.fire(
				  'Yey!',
				  `Course ${name} created successfully!`,
				  'success'
				)
				.then(function(){
					window.location = "/courses"
				})
			}else{
				Swal.fire(
				  'Oh no!',
				  'Failed in registering course!',
				  'error'
				)
			}
		})
	}



	return(
    	<Form onSubmit={(e) => createCourse(e)}>
    		<Form.Group>
    				<Form.Label>Name:</Form.Label>
    				<Form.Control 
    					type="text"
    					placeholder= "Enter Course Name"
    					value={name}
    					onChange={e => setName(e.target.value)}
    					required
    					/>
    		</Form.Group>
    		<Form.Group>
    				<Form.Label>Description:</Form.Label>
    				<Form.Control 
    					type="text"
    					placeholder= "Enter Course Description"
    					value={description}
    					onChange={e => setDescription(e.target.value)}
    					required
    					/>
    		</Form.Group>
    		<Form.Group>
    				<Form.Label>Price:</Form.Label>
    				<Form.Control 
    					type="number"
    					placeholder= "Enter Course Price"
    					value={price}
    					onChange={e => setPrice(e.target.value)}
    					required
    					/>
    		</Form.Group>
    		<Button type="submit">Save</Button>
    	</Form>
	     
		)
}