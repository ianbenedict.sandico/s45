import { Fragment, useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Register(){
	const { user } = useContext(UserContext);

	//State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');

	const [isActive, setIsActive] = useState(false);

	//Check if values are successfully binded
	// console.log(email)
	// console.log(password1)
	// console.log(password2)
	// console.log(firstName)
	// console.log(lastName)
	// console.log(mobileNo)

	useEffect(()=>{
		//Validation to enable submit button when all fields are populated and both passwords match
		if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		}else{
			setIsActive(false);
		}

	}, [email, password1, password2])

	function registerUser(e){
		e.preventDefault();
		
		fetch('http://localhost:4000/users/checkEmail',{
			method:'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire(
				  'Oh no!',
				  'Email exists!',
				  'error'
				)
			}else{
				fetch('http://localhost:4000/users/register',{
					method:'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						 lastName: lastName,
						 email: email,
						 mobileNo: mobileNo,
						 password: password1
						})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
				})

			

				Swal.fire(
				  'Yey!',
				  'You have registered!',
				  'success'
				)
			}
		})
		//To clear out the data in our fields
		setEmail('')
		setPassword1('')
		setPassword2('')
		setFirstName('')
		setLastName('')
		setMobileNo('')
	}



	//Two way binding
	//The values in the fields of the form is bound to the get the state and the event is bound to the setter. This is called two way binding
	//The data changed in the view has updated the state
	//The data in the state has updated the view

	return(
	(user.accessToken !== null)?
	<Redirect to="/"/>
	:
	<Fragment>
	<h1>Register</h1>
	<Form onSubmit={(e) => registerUser(e)}>
		<Form.Group>
				<Form.Label>Firstname:</Form.Label>
				<Form.Control 
					type="text"
					placeholder= "Enter your First name"
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
					required
					/>
		</Form.Group>
		<Form.Group>
				<Form.Label>Lastname:</Form.Label>
				<Form.Control 
					type="text"
					placeholder= "Enter your Last name"
					value={lastName}
					onChange={e => setLastName(e.target.value)}
					required
					/>
		</Form.Group>
		<Form.Group>
				<Form.Label>MobileNo:</Form.Label>
				<Form.Control 
					type="text"
					placeholder= "Enter your Mobile number"
					value={mobileNo}
					onChange={e => setMobileNo(e.target.value)}
					required
					maxLength="11"
					/>
		</Form.Group>
		<Form.Group>
				<Form.Label>Email Address:</Form.Label>
				<Form.Control 
					type="email"
					placeholder= "Enter email"
					value={email}
					onChange={e => setEmail(e.target.value)} //The e.target.value property allows us to gain access to the input field's current value to be used when submitting form data
					required
					/>
				<Form.Text className="text-muted">
					We'll never share you email with anyone else.
				</Form.Text>
		</Form.Group>
		<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder= "Enter your password"
					value={password1}
					onChange={e => setPassword1(e.target.value)}
					required
					/>
		</Form.Group>
		<Form.Group>
				<Form.Label>Verify Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder= "Verify your password"
					value={password2}
					onChange={e => setPassword2(e.target.value)}
					required
					/>
		</Form.Group>
		{isActive ?
		<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
		:
		<Button variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
		}
	</Form>
	</Fragment>
	
		)
}