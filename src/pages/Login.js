import { Fragment, useState, useEffect, useContext } from 'react'
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2'
//React Context
import UserContext from '../UserContext';
//Routing
import { Redirect, useHistory } from 'react-router-dom';


export default function Login() {
		//The useHistory hook gives you access to the history instance that you may use to navigate/toaccess the location
		//push
		const history = useHistory();

		//useContext is react hook used to unwrap our context. It will return the data passed as values by a provider(UserContext.Provider component in App.js)
		const { user, setUser} = useContext(UserContext)

		const [email, setEmail] = useState('')
		const [password, setPassword] = useState('')

		const[loginButton, setLoginButton] = useState(false)

		useEffect(() =>{

			if(email !== '' && password !==''){
				setLoginButton(true)
			}else{	
				setLoginButton(false)
			}

		}, [email, password])
		
		function login(e){
			e.preventDefault();

			//fetch has
			//1. URL from the server(routes)
			//2.Optional object which contains additional information about our requests such as method, request, body etc.
			fetch('http://localhost:4000/users/login',  {
				method:'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					email: email,
					password: password
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)
				if(data.accessToken !== undefined){
					localStorage.setItem('accessToken', data.accessToken);
					setUser({ accessToken: data.accessToken })

					Swal.fire({
						title: 'Yaaaaaay!',
						icon: 'success',
						text: 'Thank you for logging in to Zuitt Booking System'
					})

					//get user's details from our token
					fetch('http://localhost:4000/users/details', {
						headers: {
							Authorization: `Bearer ${data.accessToken}`
						}
					})
					.then(res => res.json())
					.then(data => {
						console.log(data)
						if(data.isAdmin === true){
							localStorage.setItem('email', data.email)
							localStorage.setItem('isAdmin', data.isAdmin)
							setUser({
								email: data.email,
								isAdmin: data.isAdmin
							})
							//If admin, redirect the page to /courses
							//push(path) - pushes a new entry onto history stack
							history.push('/courses')
						}else{
							//If not admin, redirect to homepage
							history.push('/')	
						}
					})

				}else{
					Swal.fire({
						title: 'Ooooops!',
						icon:'error',
						text: 'Something went wrong. Check your Credentials'
					})
				}
				setEmail('')
				setPassword('')

			})

			// Swal.fire({
			// 	title:"Yaaaaay!",
			// 	icon:"success",
			// 	text:"Login successful"
			// })
			//local storage allows us to save data within our browsers as strings
			//The setItem() method of the Storage interface, when passed a key name and value, will add that key to the given storage object, or update the key's value of if it already exists
			//setItem is used to store data in the local storage as a string
			//setItem('key', value)
			// localStorage.setItem('email',email);
			// setUser({ email: email })

			// setEmail('')
			// setPassword('')
		}

		//Create a conditional statement that will redirect the user to the homepage when a user is logged in.
		return(
			(user.accessToken !== null) ?
				<Redirect to="/"/>
				:
		<Fragment>
		<h1>Login here</h1>
			<Form onSubmit={e => login(e)}>
				<Form.Group>
					<Form.Label>Email Address:</Form.Label>	
					<Form.Control
					type="email"
					placeholder="Enter your email"
					value={email}
					onChange={e=> setEmail(e.target.value)}
					required
					/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password:</Form.Label>	
					<Form.Control
					type="password"
					placeholder="Enter your Password"
					value={password}
					onChange={e=> setPassword(e.target.value)}
					required
					/>	
				</Form.Group>
				
				{loginButton ?
				<Button variant="primary" type="submit">Submit</Button>
				:
				<Button variant="primary" type="submit" disabled>Submit</Button>
				}
				
			</Form>
			</Fragment>
		)
}