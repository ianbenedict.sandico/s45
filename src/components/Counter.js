// import {useState, useEffect, Fragment} from 'react'
// import {button} from 'react-bootstrap';
// //What does useEffect do? By using this hook, you tell React your component/elements need to do something after render. React will remember the function you passed (we''ll refer to this as our 'side effect' and call it later after performing the DOM updates)

// //Syntax of useEffect

// /*
// useEffect(()=> {}, [])

// */


// // useEffect allows us to perform tasks or function on initial render.
// // 		-when the component is displayed for the first time

// // what allows us to control when our useEffect will run AFTER the initial render?
// // 		-we add an optional dependency ARRAY to control when useEffect will run, instead that it runs on initial render AND when states are updated, we can control the useEffect to run only when the states/state in the dependency array is updated.


// export default function Counter() {
// 	const[count, setCount] = useState(0)

// 	useEffect(()=> {
// 		document.title = `You Clicked ${count} times`
// 	}, [count])

// 	return(
// 		<Fragment>
// 			<p>You Clicked {count}</p>
// 			<button variant="primary" onClick={()=> setCount(count + 1)} >Click me</button>
// 		</Fragment>
// 		)
// }