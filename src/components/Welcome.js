import { Fragment } from 'react';

//Props is for parent to child @ Home.Js
export default function Welcome(props){
	return(
		<Fragment>
			<h1>Hello, {props.kahitAno} age:{props.age} </h1>
			<p>Welcome to our Course Booking!</p>
		</Fragment>
		)
}