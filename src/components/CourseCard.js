import {useState, useEffect} from "react";
import {Button, Card} from 'react-bootstrap';
import PropTypes from 'prop-types';

//Not Destructured
// export default function CourseCard(props){

//Destructured
//To destructure, put {courseProp} <-(can be seen in Courses.js) in variable, and omit courseProp

export default function CourseCard({courseProp}){
// console.log(courseProp)
//To Further Destructure
const {description, price, name} = courseProp;

//use the state hook for this component to be able to store its state
//States are used to keep track of information related to individual component
//Syntax
	//const [getter, setter] = useState(initialGetterValue)
	//getter = stored initial or default value
	//Setter = updated value

const [count, setCount] = useState(0)
const [seats, setSeats] = useState(10)
//State hook that indicates the button for enrollment
const [isOpen, setIsOpen] = useState(true)

useEffect(() => {
	if(seats === 0){
		setIsOpen(false);
	}
}, [seats])


// console.log(useState(0))

function enroll(){

	if(seats > 0){
	setCount(count + 1);
		// console.log('Enrollees:' + count);
		setSeats(seats - 1);
		// console.log('Seats: ' + count);
	} else {
		alert("No more seats")
	
	}
}

//Check to see if the data was successfully passed
// console.log(props)
// console.log(typeof props)

	return(
			<Card className="courseHighlight p-3">
				<Card.Body>
					<Card.Title><h3>{name}</h3></Card.Title>
					<h5>Description:</h5>
					<Card.Text>
						{description}
					</Card.Text>
					<h6>Price:</h6>
					<Card.Text><h6>{price}</h6></Card.Text>
					<Card.Text>Enrollees: {count}</Card.Text>
					<Card.Text>AvailableSeats:{seats}</Card.Text>
					{isOpen ? 
					<button variant="primary" onClick={enroll}>Enroll Now</button>
					:
					
					<button variant="primary" disabled>Enroll Now</button>
					}
				</Card.Body>
			</Card>
	)
}

//Check if the CourseCard component is getting the correct prop types
//Proptypes are used for validating information passsed to a component and is a tool normally used to help developers ensure the correct information is passed from one component to the next.
//We use .notation (.propTypes) to get an object inside a package or method
CourseCard.propTypes ={
	//The "shape" method is used to check if a prop object conforms to a specific 'shape'
	courseProp: PropTypes.shape({
		//define the properties and expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}

/*
Life Cycle of components in react js

mounting> updating>unmounting

Mounting> rendering> re rendering > unmounting 

login page(mounting)> typing data in inputs (rendering)(re rendering)> home(unmounting)

*/